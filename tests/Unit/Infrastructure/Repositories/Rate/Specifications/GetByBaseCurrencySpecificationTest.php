<?php

declare(strict_types=1);

namespace Justenj\Commissions\Tests\Unit\Infrastructure\Repositories\Rate;

use Justenj\Commissions\Infrastructure\Repositories\Country\Specifications\GetByBinSpecification;
use Justenj\Commissions\Infrastructure\Repositories\Rate\Specifications\GetByBaseCurrencySpecification;
use Justenj\Commissions\Tests\TestCase;

class GetByBaseCurrencySpecificationTest extends TestCase
{
    /** @test */
    public function should_return_base_currency_in_array()
    {
        $currency = 'DKK';
        $specification = new GetByBaseCurrencySpecification($currency);

        $this->assertSame(['baseCurrency' => $currency], $specification->specify([]));
    }
}