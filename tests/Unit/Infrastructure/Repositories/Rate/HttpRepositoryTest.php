<?php

declare(strict_types=1);

namespace Justenj\Commissions\Tests\Unit\Infrastructure\Repositories\Rate;

use GuzzleHttp\ClientInterface;
use Justenj\Commissions\Application\Repositories\SpecificationContract;
use Justenj\Commissions\Domain\Exchange\Rate;
use Justenj\Commissions\Infrastructure\Repositories\Rate\HttpRepository;
use Justenj\Commissions\Tests\Unit\Infrastructure\Repositories\AbstractHttpRepositoryTest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

class HttpRepositoryTest extends AbstractHttpRepositoryTest
{
    /** @test */
    public function should_return_PHP_generator()
    {
        $client = $this->getMockBuilder(ClientInterface::class)->getMock();
        $client->expects($this->at(0))->method('getConfig')->willReturn([]);

        $content = '{"rates": {"DKK": 2}, "base": "EUR"}';
        $body = $this->getMockBuilder(StreamInterface::class)->getMock();
        $body->expects($this->atMost(1))->method('getContents')->willReturn($content);

        $response = $this->getMockBuilder(ResponseInterface::class)->getMock();
        $response->expects($this->atMost(1))->method('getBody')->willReturn($body);

        $client->expects($this->at(1))->method('request')->with('GET', '?base=EUR')->willReturn($response);

        $repository = new HttpRepository($client);

        $specification = $this->getMockBuilder(SpecificationContract::class)->getMock();
        $specification->expects($this->once())->method('specify')->with([])->willReturn(['baseCurrency' => 'EUR']);

        $iterator = $repository->query($specification);
        $rate = $iterator->current();

        $this->assertInstanceOf(\ArrayIterator::class, $iterator);
        $this->assertEquals(new Rate('DKK', 'EUR', 2), $rate);
    }

    public function getRepositoryClass()
    {
        return HttpRepository::class;
    }
}