<?php

declare(strict_types=1);

namespace Justenj\Commissions\Tests\Unit\Infrastructure\Repositories\Rate;

use Justenj\Commissions\Infrastructure\Repositories\Rate\SpecificationFactory;
use Justenj\Commissions\Infrastructure\Repositories\Rate\Specifications\GetByBaseCurrencySpecification;
use Justenj\Commissions\Tests\TestCase;

class SpecificationFactoryTest extends TestCase
{
    /** @test */
    public function should_create_specification()
    {
        $factory = new SpecificationFactory();

        $currency = 'USD';
        $specification = $factory->createGetByBaseCurrencySpecification('USD');

        $this->assertEquals(new GetByBaseCurrencySpecification($currency), $specification);
    }
}