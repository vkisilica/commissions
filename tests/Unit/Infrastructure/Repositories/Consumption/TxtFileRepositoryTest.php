<?php

declare(strict_types=1);

namespace Justenj\Commissions\Tests\Unit\Infrastructure\Repositories\Consumption;

use Justenj\Commissions\Application\Repositories\ConnectException;
use Justenj\Commissions\Application\Repositories\Country\RepositoryContract;
use Justenj\Commissions\Application\Repositories\Country\SpecificationFactoryContract;
use Justenj\Commissions\Application\Repositories\SpecificationContract;
use Justenj\Commissions\Infrastructure\Repositories\Consumption\TxtFileRepository;
use Justenj\Commissions\Tests\TestCase;
use Iterator;

class TxtFileRepositoryTest extends TestCase
{
    /** @test */
    public function should_return_PHP_generator()
    {
        $countryRepository = $this->getMockBuilder(RepositoryContract::class)->getMock();
        $specificationFactory = $this->getMockBuilder(SpecificationFactoryContract::class)->getMock();
        $specification = $this->getMockBuilder(SpecificationContract::class)->getMock();

        $repository = new TxtFileRepository($countryRepository, $specificationFactory);

        $results = $repository->query($specification);

        $this->assertInstanceOf(Iterator::class, $results);
    }

    /** @test */
    public function should_throw_exception_if_file_does_not_exist()
    {
        $inputFilePath = '../Assets/input1.txt';

        $countryRepository = $this->getMockBuilder(RepositoryContract::class)->getMock();
        $specificationFactory = $this->getMockBuilder(SpecificationFactoryContract::class)->getMock();
        $specification = $this->getMockBuilder(SpecificationContract::class)->getMock();
        $specification->expects($this->once())->method('specify')->with([])->willReturn(['filePath' => $inputFilePath]);

        $repository = new TxtFileRepository($countryRepository, $specificationFactory);

        try {
            $results = $repository->query($specification);
            foreach ($results as $result) {}
        } catch (ConnectException $e) {
            $this->assertEquals('File "../Assets/input1.txt" is not found', $e->getMessage());
            return;
        }

        $this->fail('Exception was not thrown');
    }
}