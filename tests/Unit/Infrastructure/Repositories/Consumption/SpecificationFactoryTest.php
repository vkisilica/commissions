<?php

declare(strict_types=1);

namespace Justenj\Commissions\Tests\Unit\Infrastructure\Repositories\Consumption;

use Justenj\Commissions\Infrastructure\Repositories\Consumption\SpecificationFactory;
use Justenj\Commissions\Infrastructure\Repositories\Consumption\Specifications\GetAllFromSourceSpecification;
use Justenj\Commissions\Tests\TestCase;

class SpecificationFactoryTest extends TestCase
{
    /** @test */
    public function should_create_specification()
    {
        $factory = new SpecificationFactory();

        $source = 'some source';
        $specification = $factory->createGetAllFromSourceSpecification($source);

        $this->assertEquals(new GetAllFromSourceSpecification($source), $specification);
    }
}