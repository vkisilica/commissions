<?php

declare(strict_types=1);

namespace Justenj\Commissions\Tests\Unit\Infrastructure\Repositories\Consumption;

use Justenj\Commissions\Infrastructure\Repositories\Consumption\Specifications\GetAllFromSourceSpecification;
use Justenj\Commissions\Tests\TestCase;

class GetAllFromSourceSpecificationTest extends TestCase
{
    /** @test */
    public function should_return_file_path_in_array()
    {
        $source = 'some source';
        $specification = new GetAllFromSourceSpecification($source);

        $this->assertSame(['filePath' => $source], $specification->specify([]));
    }
}