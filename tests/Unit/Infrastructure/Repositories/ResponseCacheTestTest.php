<?php

declare(strict_types=1);

namespace Justenj\Commissions\Tests\Unit\Infrastructure\Repositories;

use ArrayIterator;
use Cache\Adapter\Common\Exception\InvalidArgumentException;
use Justenj\Commissions\Application\Repositories\Country\RepositoryContract;
use Justenj\Commissions\Application\Repositories\SpecificationContract;
use Justenj\Commissions\Infrastructure\Repositories\ResponseCache;
use Justenj\Commissions\Tests\TestCase;
use Psr\SimpleCache\CacheInterface;

class ResponseCacheTest extends TestCase
{
    /** @test */
    public function should_call_repository_if_data_is_not_cached()
    {
        $expectedIterator = new ArrayIterator();
        $specification = $this->getMockBuilder(SpecificationContract::class)->getMock();
        $repository = $this->getMockBuilder(RepositoryContract::class)->getMock();
        $repository->expects($this->once())->method('query')->with($specification)->willReturn($expectedIterator);

        $cache = $this->getMockBuilder(CacheInterface::class)->getMock();
        $cache->expects($this->once())->method('has')->willReturn(false);
        $cache->expects($this->never())->method('get');
        $cache->expects($this->once())->method('set')->with(sha1(serialize($specification)), serialize($expectedIterator));

        $responseCache = new ResponseCache($repository, $cache);
        $actualIterator = $responseCache->query($specification);

        $this->assertSame($actualIterator, $expectedIterator);
    }

    /** @test */
    public function should_retrieve_data_from_cache()
    {
        $expectedIterator = new ArrayIterator();
        $specification = $this->getMockBuilder(SpecificationContract::class)->getMock();
        $repository = $this->getMockBuilder(RepositoryContract::class)->getMock();

        $cache = $this->getMockBuilder(CacheInterface::class)->getMock();
        $cache->expects($this->once())->method('has')->willReturn(true);
        $cache->expects($this->once())->method('get')->with(sha1(serialize($specification)))->willReturn(serialize($expectedIterator));
        $cache->expects($this->never())->method('set');

        $responseCache = new ResponseCache($repository, $cache);
        $actualIterator = $responseCache->query($specification);

        $this->assertEquals($actualIterator, $expectedIterator);
    }

    /** @test */
    public function should_suppress_exceptions()
    {
        $expectedIterator = new ArrayIterator();
        $specification = $this->getMockBuilder(SpecificationContract::class)->getMock();
        $repository = $this->getMockBuilder(RepositoryContract::class)->getMock();
        $repository->expects($this->once())->method('query')->with($specification)->willReturn($expectedIterator);

        $cache = $this->getMockBuilder(CacheInterface::class)->getMock();
        $cache->expects($this->once())->method('has')->willThrowException(new InvalidArgumentException());
        $cache->expects($this->never())->method('get');
        $cache->expects($this->once())->method('set')->with(sha1(serialize($specification)), serialize($expectedIterator));

        $responseCache = new ResponseCache($repository, $cache);
        $actualIterator = $responseCache->query($specification);

        $this->assertEquals($actualIterator, $expectedIterator);
    }
}