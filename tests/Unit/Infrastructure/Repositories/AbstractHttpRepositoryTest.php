<?php

declare(strict_types=1);

namespace Justenj\Commissions\Tests\Unit\Infrastructure\Repositories;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Psr7\Request;
use Justenj\Commissions\Application\Repositories\SpecificationContract;
use Justenj\Commissions\Tests\TestCase;

abstract class AbstractHttpRepositoryTest extends TestCase
{
    abstract public function getRepositoryClass();

    /**
     * @test
     * @dataProvider exceptionsProvider
     */
    public function should_throw_connect_exception_when_guzzle_exception_is_thrown($exception, $exceptionClass)
    {
        $client = $this->getMockBuilder(ClientInterface::class)->getMock();
        $client->method('request')->willThrowException($exception);
        $repositoryClass = $this->getRepositoryClass();
        $repository = new $repositoryClass($client);

        $specification = $this->getMockBuilder(SpecificationContract::class)->getMock();
        $specification->method('specify')->willReturn(['baseCurrency' => 'EUR']);
        try {
            $repository->query($specification);
        } catch (\Throwable $e) {
            $this->assertInstanceOf($exceptionClass, $e);
            $this->assertTrue(true);
            return;
        }

        $this->fail('Exception was not thrown');
    }

    public function exceptionsProvider()
    {
        return [
            [
                new \JsonException(),
                \Justenj\Commissions\Application\Repositories\InvalidDataException::class
            ],[
                new ConnectException('', $this->getMockBuilder(Request::class)->disableOriginalConstructor()->getMock()),
                \Justenj\Commissions\Application\Repositories\ConnectException::class
            ]
        ];
    }
}