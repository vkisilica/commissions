<?php

declare(strict_types=1);

namespace Justenj\Commissions\Tests\Unit\Infrastructure\Repositories;

use ArrayIterator;
use Carbon\Carbon;
use Justenj\Commissions\Application\Repositories\Country\RepositoryContract;
use Justenj\Commissions\Application\Repositories\SpecificationContract;
use Justenj\Commissions\Infrastructure\Repositories\RequestsThrottle;
use Justenj\Commissions\Tests\TestCase;

class RequestsThrottleTest extends TestCase
{
    /**
     * @test
     * @dataProvider dataProvider
     */
    public function should_throttle_requests($requests, $microseconds, $delay)
    {
        $repository = $this->getMockBuilder(RepositoryContract::class)->getMock();
        $specification = $this->getMockBuilder(SpecificationContract::class)->getMock();
        $throttle = new RequestsThrottle($repository, $requests, $microseconds);

        $firstIteration = Carbon::now();
        $throttle->query($specification);
        $throttle->query($specification);

        $secondIteration = Carbon::now();
        $this->assertGreaterThanOrEqual($delay, $secondIteration->diffInMicroseconds($firstIteration));
        $this->assertLessThan($delay * 1000, $secondIteration->diffInMicroseconds($firstIteration));

        $throttle->query($specification);
        $thirdIteration = Carbon::now();
        $this->assertGreaterThanOrEqual($delay, $thirdIteration->diffInMicroseconds($secondIteration));
        $this->assertLessThan($delay * 1000, $thirdIteration->diffInMicroseconds($secondIteration));
    }

    /** @test */
    public function should_proxy_PHP_generator()
    {
        $expectedIterator = new ArrayIterator();
        $specification = $this->getMockBuilder(SpecificationContract::class)->getMock();
        $repository = $this->getMockBuilder(RepositoryContract::class)->getMock();
        $repository->method('query')->with($specification)->willReturn($expectedIterator);
        $throttle = new RequestsThrottle($repository, 100, 1000000);
        $actualIterator = $throttle->query($specification);

        $this->assertSame($actualIterator, $expectedIterator);
    }

    public function dataProvider()
    {
        return [
            [1, $this->toMicroseconds(1), $this->toMicroseconds(1)],
            [10, $this->toMicroseconds(10), $this->toMicroseconds(1)],
            [100, $this->toMicroseconds(10), $this->toMicroseconds(0.001)]
        ];
    }

    /**
     * @param $seconds
     * @return float|int
     */
    private function toMicroseconds($seconds)
    {
        return $seconds * 1000000;
    }
}