<?php

declare(strict_types=1);

namespace Justenj\Commissions\Tests\Unit\Infrastructure\Repositories\Country;

use Justenj\Commissions\Infrastructure\Repositories\Country\SpecificationFactory;
use Justenj\Commissions\Infrastructure\Repositories\Country\Specifications\GetByBinSpecification;
use Justenj\Commissions\Tests\TestCase;

class SpecificationFactoryTest extends TestCase
{
    /** @test */
    public function should_create_specification()
    {
        $factory = new SpecificationFactory();

        $bin = '1111';
        $specification = $factory->createGetByBinSpecification($bin);

        $this->assertEquals(new GetByBinSpecification($bin), $specification);
    }
}