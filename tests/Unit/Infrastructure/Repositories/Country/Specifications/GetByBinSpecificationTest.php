<?php

declare(strict_types=1);

namespace Justenj\Commissions\Tests\Unit\Infrastructure\Repositories\Country;

use Justenj\Commissions\Infrastructure\Repositories\Country\Specifications\GetByBinSpecification;
use Justenj\Commissions\Tests\TestCase;

class GetByBinSpecificationTest extends TestCase
{
    /** @test */
    public function should_return_bin_in_array()
    {
        $bin = '1112';
        $specification = new GetByBinSpecification($bin);

        $this->assertSame(['bin' => $bin], $specification->specify([]));
    }
}