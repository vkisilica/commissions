<?php

declare(strict_types=1);

namespace Justenj\Commissions\Tests\Unit\Infrastructure\Repositories\Country;

use Justenj\Commissions\Application\Repositories\SpecificationContract;
use Justenj\Commissions\Domain\Country;
use Justenj\Commissions\Infrastructure\Repositories\Country\HttpRepository;
use Justenj\Commissions\Tests\TestCase;
use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

class HttpRepositoryTest extends TestCase
{
    /** @test */
    public function should_return_issue_country()
    {
        $client = $this->getMockBuilder(ClientInterface::class)->getMock();
        $client->expects($this->at(0))->method('getConfig')->willReturn([]);

        $content = '{"country": {"alpha2": "DK"}}';
        $body = $this->getMockBuilder(StreamInterface::class)->getMock();
        $body->expects($this->atMost(1))->method('getContents')->willReturn($content);

        $response = $this->getMockBuilder(ResponseInterface::class)->getMock();
        $response->expects($this->atMost(1))->method('getBody')->willReturn($body);

        $client->expects($this->at(1))->method('request')->with('GET', 1111)->willReturn($response);

        $repository = new HttpRepository($client);

        $specification = $this->getMockBuilder(SpecificationContract::class)->getMock();
        $specification->expects($this->once())->method('specify')->with([])->willReturn(['bin' => 1111]);

        $iterator = $repository->query($specification);
        $issueCountry = $iterator->current();

        $this->assertInstanceOf(\ArrayIterator::class, $iterator);
        $this->assertEquals(Country::fromAlpha2('DK'), $issueCountry);
    }


    public function getRepositoryClass()
    {
        return HttpRepository::class;
    }
}