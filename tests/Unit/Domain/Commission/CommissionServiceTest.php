<?php

declare(strict_types=1);

namespace Justenj\Commissions\Tests\Unit\Domain\Commission;

use Evp\Component\Money\Money;
use Justenj\Commissions\Domain\Commission\CommissionService;
use Justenj\Commissions\Domain\Consumption;
use Justenj\Commissions\Domain\Exchange\ExchangeService;
use Justenj\Commissions\Domain\Exchange\Rate;
use Justenj\Commissions\Tests\TestCase;

class CommissionServiceTest extends TestCase
{
    private CommissionService $commissionService;

    public function setUp(): void
    {
        $iterator = new \ArrayIterator([
            new Rate('DKK', 'EUR', 1.1),
            new Rate('USD', 'EUR', 2)
        ]);
        $exchangeService = new ExchangeService($iterator);
        $this->commissionService = new CommissionService($exchangeService);
    }

    /** @test */
    public function should_not_exchange_if_money_has_eur_currency()
    {
        $consumption = Consumption::fromArray([
            'bin' => '111111',
            'country' => 'DK',
            'amount' => 15,
            'currency' => 'EUR'
        ]);
        $commission = $this->commissionService->calculate($consumption);

        $expectedCommission = new Money(0.15, 'EUR');
        $this->assertEquals($expectedCommission, $commission);
    }

    /** @test */
    public function should_exchange_if_money_does_not_have_eur_currency_but_eu_country()
    {
        $consumption = Consumption::fromArray([
            'bin' => '111111',
            'country' => 'DK',
            'amount' => 1.1,
            'currency' => 'DKK'
        ]);
        $commission = $this->commissionService->calculate($consumption);

        $expectedCommission = new Money(0.01, 'EUR');
        $this->assertEquals($expectedCommission, $commission);
    }

    /** @test */
    public function should_exchange_if_money_does_not_have_eur_currency_and_not_eu_country()
    {
        $consumption = Consumption::fromArray([
            'bin' => '111111',
            'country' => 'US',
            'amount' => 2,
            'currency' => 'USD'
        ]);
        $commission = $this->commissionService->calculate($consumption);

        $expectedCommission = new Money(0.02, 'EUR');
        $this->assertEquals($expectedCommission, $commission);
    }
}