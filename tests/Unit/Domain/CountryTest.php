<?php

declare(strict_types=1);

namespace Justenj\Commissions\Tests\Unit\Domain;

use Justenj\Commissions\Domain\Country;
use Justenj\Commissions\Tests\TestCase;
use InvalidArgumentException;

class CountryTest extends TestCase
{
    /**
     * @test
     * @dataProvider validData
     */
    public function creating($alpha2)
    {
        $this->assertInstanceOf(Country::class, Country::fromAlpha2($alpha2));
    }

    /**
     * @test
     * @dataProvider invalidValues
     */
    public function creating_exception($alpha2, $message)
    {
        try {
            Country::fromAlpha2($alpha2);
        } catch (InvalidArgumentException $e) {
            $this->assertEquals($message, $e->getMessage());
            return;
        }

        $this->fail('Exception was not thrown');
        $this->assertInstanceOf(Country::class, Country::fromAlpha2($alpha2));
    }

    /** @test */
    public function is_eu()
    {
        $this->assertTrue(Country::fromAlpha2('DK')->isEU());
        $this->assertFalse(Country::fromAlpha2('AA')->isEU());
    }

    public function validData()
    {
        return [
            ['DE'],
            ['DK'],
            ['UK']
        ];
    }

    public function invalidValues()
    {
        return [
            ['U', 'Expected a value to contain 2 characters. Got: "U"'],
            ['AAA', 'Expected a value to contain 2 characters. Got: "AAA"']
        ];
    }


}