<?php

declare(strict_types=1);

namespace Justenj\Commissions\Tests\Unit\Domain\Exchange;

use ArrayIterator;
use Evp\Component\Money\Money;
use Justenj\Commissions\Domain\Exchange\ExchangeRateNotDefinedException;
use Justenj\Commissions\Domain\Exchange\ExchangeService;
use Justenj\Commissions\Domain\Exchange\Rate;
use Justenj\Commissions\Tests\TestCase;

class ExchangeServiceTest extends TestCase
{
    private ExchangeService $exchangeService;

    public function setUp(): void
    {
        $arrayIterator = new ArrayIterator([new Rate('EUR', 'USD', 0.9)]);
        $this->exchangeService = new ExchangeService($arrayIterator);
    }

    /** @test */
    public function should_return_same_money_if_currency_is_same()
    {
        $money = new Money(10, 'EUR');
        $exchangedMoney = $this->exchangeService->exchange($money, 'EUR');
        $this->assertSame($exchangedMoney, $money);
    }

    /** @test */
    public function should_return_exchanged_money()
    {
        $money = new Money(90, 'EUR');
        $exchangedMoney = $this->exchangeService->exchange($money, 'USD');

        $expectedMoney = new Money(100, 'USD');
        $this->assertTrue($exchangedMoney->isEqual($expectedMoney));
    }

    /** @test */
    public function should_return_throw_exception_when_rate_is_not_defined()
    {
        $money = new Money(90, 'EUR');
        try {
            $this->exchangeService->exchange($money, 'DKK');
        } catch (ExchangeRateNotDefinedException $e) {
            $this->assertEquals('"DKK/EUR" pair is not defined', $e->getMessage());
            return;
        }

        $this->fail('Exception was not thrown');
    }
}