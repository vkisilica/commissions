<?php

declare(strict_types=1);

namespace Justenj\Commissions\Tests\Unit\Domain\Exchange;

use Justenj\Commissions\Domain\Exchange\Rate;
use Exception;
use TypeError;

class RateTest extends \Justenj\Commissions\Tests\TestCase
{
    /** @test */
    public function creating()
    {
        $this->assertInstanceOf(Rate::class, new Rate('EUR', 'USD', 1.1));
    }

    /**
     * @test
     * @dataProvider invalidData
     */
    public function invalid_creating($from, $to, $rate, $message)
    {
        try {
            $this->assertInstanceOf(Rate::class, new Rate($from, $to, $rate));
        } catch (Exception | TypeError $e) {
            $this->assertStringStartsWith($message, $e->getMessage());
            return;
        }

        $this->fail('Exception was not thrown');
    }

    public function invalidData()
    {
        return [
            ['EUR', 'DKK', '1.1', 'Argument 3 passed to Justenj\Commissions\Domain\Exchange\Rate::__construct() must be of the type float, string given'],
            ['EUR', 'DKK', 0, 'Expected a value greater than 0. Got: 0'],
            ['EUR', 'DKK', -1, 'Expected a value greater than 0. Got: -1'],
            ['EUR', 'DK', 0.1, 'Expected a value to contain 3 characters. Got: "DK"'],
            ['EU', 'DKK', 0.1, 'Expected a value to contain 3 characters. Got: "EU"'],
        ];
    }
}