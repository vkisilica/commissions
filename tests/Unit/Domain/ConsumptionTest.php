<?php

declare(strict_types=1);

namespace Justenj\Commissions\Tests\Unit\Domain;

use Justenj\Commissions\Domain\Consumption;
use Justenj\Commissions\Tests\TestCase;
use InvalidArgumentException;

class ConsumptionTest extends TestCase
{
    /** @test */
    public function creating()
    {
        $payload = [
            'bin' => '111111',
            'country' => 'DK',
            'amount' => 100.01,
            'currency' => 'EUR'
        ];

        $consumption = Consumption::fromArray($payload);
        $this->assertInstanceOf(Consumption::class, $consumption);
    }

    /**
     * @test
     * @dataProvider invalidValues
     * @param $payload
     */
    public function invalid_creating($payload, $message)
    {
        try {
            Consumption::fromArray($payload);
        } catch (InvalidArgumentException $e) {
            $this->assertEquals($message, $e->getMessage());
            return;
        }

        $this->fail('Exception was not thrown');
    }

    public function invalidValues()
    {
        return [
            [
                [
                    'country' => 'DK',
                    'amount' => 100.01,
                    'currency' => 'EUR'
                ],
                'Expected the key "bin" to exist.'
            ],[
                [
                    'bin' => '111111',
                    'amount' => 100.01,
                    'currency' => 'EUR'
                ],
                'Expected the key "country" to exist.'
            ],[
                [
                    'bin' => '111111',
                    'country' => 'DK',
                    'currency' => 'EUR'
                ],
                'Expected the key "amount" to exist.'
            ],[
                [
                    'bin' => '111111',
                    'country' => 'DK',
                    'amount' => 100.01,
                ],
                'Expected the key "currency" to exist.'
            ]
        ];
    }
}