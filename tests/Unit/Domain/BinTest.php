<?php

declare(strict_types=1);

namespace Justenj\Commissions\Tests\Unit\Domain;

use Justenj\Commissions\Domain\Bin;
use Justenj\Commissions\Tests\TestCase;
use InvalidArgumentException;

class BinTest extends TestCase
{
    /**
     * @test
     * @dataProvider validValues
     * @param $value
     */
    public function creating($value)
    {
        $bin = new Bin($value);

        $this->assertInstanceOf(Bin::class, $bin);
    }

    /**
     * @test
     * @dataProvider invalidValues
     * @param $value
     * @param $message
     */
    public function creating_exception($value, $message)
    {
        try {
            new Bin($value);
        } catch (InvalidArgumentException $e) {
            $this->assertEquals($message, $e->getMessage());
            return;
        }

        $this->fail('Exception was not thrown');
    }

    public function validValues()
    {
        return [
            ['000000'],
            ['0000000'],
            ['99999999']
        ];
    }

    public function invalidValues()
    {
        return [
            ['00000', 'Expected a value to contain at least 6 characters. Got: "00000"'],
            ['999999999', 'Expected a value to contain at most 8 characters. Got: "999999999"'],
            ['9999999s', 'Expected a value to contain digits only. Got: "9999999s"']
        ];
    }
}