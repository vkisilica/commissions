<?php

declare(strict_types=1);

namespace Justenj\Commissions\Tests\Functional\Application\UseCases;

use ArrayIterator;
use Cache\Adapter\PHPArray\ArrayCachePool;
use Iterator;
use Justenj\Commissions\Application\Repositories\Country\RepositoryContract as CountryRepositoryContract;
use Justenj\Commissions\Application\Repositories\Rate\RepositoryContract as RateRepositoryContract;
use Justenj\Commissions\Application\UseCases\CalculateCommissionsInEurosForAllConsumptions;
use Justenj\Commissions\Domain\Country;
use Justenj\Commissions\Domain\Exchange\Rate;
use Justenj\Commissions\Infrastructure\Repositories\Consumption\SpecificationFactory as ConsumptionSpecificationFactory;
use Justenj\Commissions\Infrastructure\Repositories\Consumption\TxtFileRepository;
use Justenj\Commissions\Infrastructure\Repositories\Country\SpecificationFactory as CountrySpecificationFactory;
use Justenj\Commissions\Infrastructure\Repositories\Country\Specifications\GetByBinSpecification;
use Justenj\Commissions\Infrastructure\Repositories\Rate\SpecificationFactory as RateSpecificationFactory;
use Justenj\Commissions\Infrastructure\Repositories\Rate\Specifications\GetByBaseCurrencySpecification;
use Justenj\Commissions\Infrastructure\Repositories\RequestsThrottle;
use Justenj\Commissions\Infrastructure\Repositories\ResponseCache;
use Justenj\Commissions\Tests\TestCase;

class CalculateCommissionsInEurosForAllConsumptionsTest extends TestCase
{
    /** @test */
    public function should_calculate_all_commissions()
    {
        $useCase = $this->buildUseCase();

        $results = $useCase->handle(sprintf(
            '%s%s%s%s%s%s%s%s%s%s%s',
            __DIR__,
            DIRECTORY_SEPARATOR,
            '..',
            DIRECTORY_SEPARATOR,
            '..',
            DIRECTORY_SEPARATOR,
            '..',
            DIRECTORY_SEPARATOR,
            'Assets',
            DIRECTORY_SEPARATOR,
            'input.txt'
        ));

        $this->assertInstanceOf(Iterator::class, $results);
        $this->assertEquals([
            'consumption' => [
                'bin' => '45717360',
                'issuingCountry' => [
                    'alpha2' => 'DK'
                ],
                'money' => [
                    'amount' => 100.00,
                    'currency' => 'EUR'
                ]
            ],
            'commission' => [
                'amount' => 1.00,
                'currency' => 'EUR'
            ]
        ], $results->current());

        $results->next();
        $this->assertEquals([
            'consumption' => [
                'bin' => '516793',
                'issuingCountry' => [
                    'alpha2' => 'LT'
                ],
                'money' => [
                    'amount' => 50.00,
                    'currency' => 'USD'
                ]
            ],
            'commission' => [
                'amount' => 0.25,
                'currency' => 'EUR'
            ]
        ], $results->current());

        $results->next();
        $this->assertEquals([
            'consumption' => [
                'bin' => '45417360',
                'issuingCountry' => [
                    'alpha2' => 'JP'
                ],
                'money' => [
                    'amount' => 10000.00,
                    'currency' => 'JPY'
                ]
            ],
            'commission' => [
                'amount' => 0.2,
                'currency' => 'EUR'
            ]
        ], $results->current());

        $results->next();
        $this->assertEquals([
            'consumption' => [
                'bin' => '41417360',
                'issuingCountry' => [
                    'alpha2' => 'US'
                ],
                'money' => [
                    'amount' => 130.00,
                    'currency' => 'USD'
                ]
            ],
            'commission' => [
                'amount' => 1.3,
                'currency' => 'EUR'
            ]
        ], $results->current());

        $results->next();
        $this->assertEquals([
            'consumption' => [
                'bin' => '4745030',
                'issuingCountry' => [
                    'alpha2' => 'GB'
                ],
                'money' => [
                    'amount' => 2000.00,
                    'currency' => 'GBP'
                ]
            ],
            'commission' => [
                'amount' => 13.34,
                'currency' => 'EUR'
            ]
        ], $results->current());
    }

    private function buildUseCase()
    {
        $countryRepository = $this->getMockBuilder(CountryRepositoryContract::class)->getMock(); // because of third party service

        $countryIterator = new ArrayIterator([Country::fromAlpha2('DK')]);
        $getByBinSpec = new GetByBinSpecification('45717360');
        $countryRepository->expects($this->at(0))->method('query')->with($getByBinSpec)->willReturn($countryIterator);

        $countryIterator = new ArrayIterator([Country::fromAlpha2('LT')]);
        $getByBinSpec = new GetByBinSpecification('516793');
        $countryRepository->expects($this->at(1))->method('query')->with($getByBinSpec)->willReturn($countryIterator);

        $countryIterator = new ArrayIterator([Country::fromAlpha2('JP')]);
        $getByBinSpec = new GetByBinSpecification('45417360');
        $countryRepository->expects($this->at(2))->method('query')->with($getByBinSpec)->willReturn($countryIterator);

        $countryIterator = new ArrayIterator([Country::fromAlpha2('US')]);
        $getByBinSpec = new GetByBinSpecification('41417360');
        $countryRepository->expects($this->at(3))->method('query')->with($getByBinSpec)->willReturn($countryIterator);

        $countryIterator = new ArrayIterator([Country::fromAlpha2('GB')]);
        $getByBinSpec = new GetByBinSpecification('4745030');
        $countryRepository->expects($this->at(4))->method('query')->with($getByBinSpec)->willReturn($countryIterator);

        $countrySpecFactory = new CountrySpecificationFactory();

        $requestsThrottle = new RequestsThrottle($countryRepository, 100, 60 * 1000000); // less latency because of tests
        $cache = new ArrayCachePool();
        $countryResponseCache = new ResponseCache($requestsThrottle, $cache);
        $consumptionRepository = new TxtFileRepository($countryResponseCache, $countrySpecFactory);
        $consumptionSpecFactory = new ConsumptionSpecificationFactory();

        $rateIterator = new ArrayIterator([
            new Rate('USD', 'EUR', 2),
            new Rate('JPY', 'EUR', 1000),
            new Rate('GBP', 'EUR', 3)
        ]);
        $getByBaseCurrencySpec = new GetByBaseCurrencySpecification('EUR');
        $rateRepository = $this->getMockBuilder(RateRepositoryContract::class)->getMock(); // because of third party service
        $rateRepository->method('query')->with($getByBaseCurrencySpec)->willReturn($rateIterator);

        $countryResponseCache = new ResponseCache($rateRepository, $cache);
        $rateSpecFactory = new RateSpecificationFactory();

        return new CalculateCommissionsInEurosForAllConsumptions($consumptionRepository, $consumptionSpecFactory, $countryResponseCache, $rateSpecFactory);
    }
}