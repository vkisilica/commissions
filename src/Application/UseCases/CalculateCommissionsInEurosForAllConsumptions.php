<?php

declare(strict_types=1);

namespace Justenj\Commissions\Application\UseCases;

use Iterator;
use Justenj\Commissions\Application\Repositories\Consumption\RepositoryContract as ConsumptionRepository;
use Justenj\Commissions\Application\Repositories\Consumption\SpecificationFactoryContract as ConsumptionSpecificationFactory;
use Justenj\Commissions\Application\Repositories\Rate\RepositoryContract as RateRepository;
use Justenj\Commissions\Application\Repositories\Rate\SpecificationFactoryContract as RateSpecificationFactory;
use Justenj\Commissions\Domain\Commission\CommissionService;
use Justenj\Commissions\Domain\Exchange\ExchangeService;

class CalculateCommissionsInEurosForAllConsumptions
{
    private ConsumptionRepository $consumptionRepository;
    private ConsumptionSpecificationFactory $consumptionSpecificationFactory;
    private RateRepository $rateRepository;
    private RateSpecificationFactory $rateSpecificationFactory;

    public function __construct(
        ConsumptionRepository $consumptionRepository,
        ConsumptionSpecificationFactory $consumptionSpecificationFactory,
        RateRepository $rateRepository,
        RateSpecificationFactory $rateSpecificationFactory
    ) {
        $this->consumptionRepository = $consumptionRepository;
        $this->consumptionSpecificationFactory = $consumptionSpecificationFactory;
        $this->rateRepository = $rateRepository;
        $this->rateSpecificationFactory = $rateSpecificationFactory;
    }

    /**
     * @param mixed $consumptionsSource
     * @return Iterator
     */
    public function handle($consumptionsSource): Iterator
    {
        $rates = $this->rateRepository->query(
            $this->rateSpecificationFactory->createGetByBaseCurrencySpecification('EUR')
        );

        $exchangeService = new ExchangeService($rates); // don't have to use open/close principle here
        $commissionService = new CommissionService($exchangeService); // don't have to use open/close principle here

        $consumptions = $this->consumptionRepository->query(
            $this->consumptionSpecificationFactory->createGetAllFromSourceSpecification($consumptionsSource)
        );
        /** @var \Justenj\Commissions\Domain\Consumption $consumption */
        foreach ($consumptions as $consumption) {
            yield [
                'consumption' => $consumption->getArrayRepresentation(),
                'commission' => $commissionService->calculate($consumption)->getArrayRepresentation()
            ];
        }
    }
}