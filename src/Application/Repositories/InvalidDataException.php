<?php

declare(strict_types=1);

namespace Justenj\Commissions\Application\Repositories;

use UnexpectedValueException;

/** @note: Make exception class and define in interface because exceptions should not depend on implementations. */
class InvalidDataException extends UnexpectedValueException
{

}