<?php

declare(strict_types=1);

namespace Justenj\Commissions\Application\Repositories\Rate;

use Justenj\Commissions\Application\Repositories\SpecificationContract;

interface SpecificationFactoryContract
{
    public function createGetByBaseCurrencySpecification(string $currency): SpecificationContract;
}