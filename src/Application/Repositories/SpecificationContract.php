<?php

declare(strict_types=1);

namespace Justenj\Commissions\Application\Repositories;

interface SpecificationContract
{
    /**
     * @param mixed $payload
     * @return mixed
     */
    public function specify($payload);
}