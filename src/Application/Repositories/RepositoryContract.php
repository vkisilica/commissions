<?php

declare(strict_types=1);

namespace Justenj\Commissions\Application\Repositories;

use Iterator;

interface RepositoryContract
{
    /**
     * @param SpecificationContract $specification
     * @return Iterator
     * @throws ConnectException
     * @throws InvalidDataException
     */
    public function query(SpecificationContract $specification): Iterator;
}