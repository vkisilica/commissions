<?php

declare(strict_types=1);

namespace Justenj\Commissions\Application\Repositories\Country;

use Justenj\Commissions\Application\Repositories\SpecificationContract;

interface SpecificationFactoryContract
{
    public function createGetByBinSpecification(string $bin): SpecificationContract;
}