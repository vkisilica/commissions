<?php

declare(strict_types=1);

namespace Justenj\Commissions\Application\Repositories\Consumption;

use Justenj\Commissions\Application\Repositories\SpecificationContract;

interface SpecificationFactoryContract
{
    public function createGetAllFromSourceSpecification($source): SpecificationContract;
}