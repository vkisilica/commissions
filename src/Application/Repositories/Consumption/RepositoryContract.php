<?php

declare(strict_types=1);

namespace Justenj\Commissions\Application\Repositories\Consumption;

interface RepositoryContract extends \Justenj\Commissions\Application\Repositories\RepositoryContract
{

}