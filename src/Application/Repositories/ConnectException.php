<?php

declare(strict_types=1);

namespace Justenj\Commissions\Application\Repositories;

use OutOfBoundsException;

/** @note: Make exception class and define in interface because exceptions should not depend on implementations. */
class ConnectException extends OutOfBoundsException
{

}