<?php

declare(strict_types=1);

namespace Justenj\Commissions\Infrastructure\Repositories\Rate\Specifications;

use Justenj\Commissions\Application\Repositories\SpecificationContract;

class GetByBaseCurrencySpecification implements SpecificationContract
{
    private string $baseCurrency;

    public function __construct(string $baseCurrency)
    {
        $this->baseCurrency = $baseCurrency;
    }

    /**
     * @param array $payload
     * @return array
     */
    public function specify($payload)
    {
        $payload['baseCurrency'] = $this->baseCurrency;
        return $payload;
    }
}