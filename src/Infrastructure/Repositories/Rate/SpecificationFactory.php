<?php

declare(strict_types=1);

namespace Justenj\Commissions\Infrastructure\Repositories\Rate;

use Justenj\Commissions\Application\Repositories\Rate\SpecificationFactoryContract;
use Justenj\Commissions\Application\Repositories\SpecificationContract;
use Justenj\Commissions\Infrastructure\Repositories\Rate\Specifications\GetByBaseCurrencySpecification;

class SpecificationFactory implements SpecificationFactoryContract
{
    public function createGetByBaseCurrencySpecification(string $currency): SpecificationContract
    {
        return new GetByBaseCurrencySpecification($currency);
    }
}