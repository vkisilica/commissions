<?php

declare(strict_types=1);

namespace Justenj\Commissions\Infrastructure\Repositories\Rate;

use ArrayIterator;
use Iterator;
use InvalidArgumentException;
use Justenj\Commissions\Application\Repositories\ConnectException;
use Justenj\Commissions\Domain\Exchange\Rate;
use Justenj\Commissions\Infrastructure\Repositories\AbstractHttpRepository;
use Psr\Http\Message\ResponseInterface;

class HttpRepository extends AbstractHttpRepository
{
    /** @inheritDoc */
    protected function validate(array $queryOptions): void
    {
        if (!array_key_exists('baseCurrency', $queryOptions)) {
            throw new InvalidArgumentException('$queryOptions should contain the "baseCurrency" key');
        }
    }

    /** @inheritDoc */
    protected function request($payload): ResponseInterface
    {
        return $this->client->request('GET', '?base=' . $payload['baseCurrency']);
    }

    /** @inheritDoc */
    protected function convertToIterator(ResponseInterface $response, $queryOptions): Iterator
    {
        $body = json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);

        $ratesKeyExists = array_key_exists('rates', $body);
        $baseKeyExists = array_key_exists('base', $body);
        $baseCurrencyIsCorrect = $body['base'] === $queryOptions['baseCurrency'];
        if (!$ratesKeyExists || !$baseKeyExists || !$baseCurrencyIsCorrect) {
            throw new ConnectException('The service returned invalid response');
        }

        $iterator = new ArrayIterator();
        foreach ($body['rates'] as $fromCurrency => $rate) {
            $iterator->append(new Rate($fromCurrency, $body['base'], (float)$rate));
        }

        return $iterator;
    }
}