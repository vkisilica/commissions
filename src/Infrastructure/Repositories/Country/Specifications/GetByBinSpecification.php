<?php

declare(strict_types=1);

namespace Justenj\Commissions\Infrastructure\Repositories\Country\Specifications;

use Justenj\Commissions\Application\Repositories\SpecificationContract;

class GetByBinSpecification implements SpecificationContract
{
    private string $bin;

    public function __construct(string $bin)
    {
        $this->bin = $bin;
    }

    /**
     * @param array $payload
     * @return array
     */
    public function specify($payload)
    {
        $payload['bin'] = $this->bin;
        return $payload;
    }
}