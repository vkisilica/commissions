<?php

declare(strict_types=1);

namespace Justenj\Commissions\Infrastructure\Repositories\Country;

use ArrayIterator;
use GuzzleHttp\ClientInterface;
use Iterator;
use InvalidArgumentException;
use Justenj\Commissions\Application\Repositories\ConnectException;
use Justenj\Commissions\Application\Repositories\Country\RepositoryContract;
use Justenj\Commissions\Domain\Country;
use Justenj\Commissions\Infrastructure\Repositories\AbstractHttpRepository;
use Psr\Http\Message\ResponseInterface;

class HttpRepository extends AbstractHttpRepository implements RepositoryContract
{
    public function __construct(ClientInterface $client)
    {
        parent::__construct($client);
    }

    /** @inheritDoc */
    protected function validate(array $queryOptions): void
    {
        if (!array_key_exists('bin', $queryOptions)) {
            throw new InvalidArgumentException('Payload should contain BIN');
        }
    }

    /**
     * @inheritDoc
     */
    protected function request(array $queryOptions): ResponseInterface
    {
        $bin = $queryOptions['bin'];
        return $this->client->request('GET', $bin);
    }

    /** @inheritDoc */
    protected function convertToIterator(ResponseInterface $response, array $queryOptions): Iterator
    {
        $body = json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);

        $countryKeyExists = array_key_exists('country', $body);
        $alpha2KeyExists = $countryKeyExists && array_key_exists('alpha2', $body['country']);
        if (!$alpha2KeyExists) {
            throw new ConnectException('The service returned invalid response');
        }
        $country = Country::fromAlpha2($body['country']['alpha2']);

        return new ArrayIterator([$country]);
    }
}