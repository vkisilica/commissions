<?php

declare(strict_types=1);

namespace Justenj\Commissions\Infrastructure\Repositories\Country;

use Justenj\Commissions\Application\Repositories\Country\SpecificationFactoryContract;
use Justenj\Commissions\Application\Repositories\SpecificationContract;
use Justenj\Commissions\Infrastructure\Repositories\Country\Specifications\GetByBinSpecification;

class SpecificationFactory implements SpecificationFactoryContract
{
    public function createGetByBinSpecification(string $bin): SpecificationContract
    {
        return new GetByBinSpecification($bin);
    }
}