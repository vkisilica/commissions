<?php

declare(strict_types=1);

namespace Justenj\Commissions\Infrastructure\Repositories;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Iterator;
use InvalidArgumentException;
use Justenj\Commissions\Application\Repositories\ConnectException;
use JsonException;
use Justenj\Commissions\Application\Repositories\Country\RepositoryContract;
use Justenj\Commissions\Application\Repositories\InvalidDataException;
use Justenj\Commissions\Application\Repositories\SpecificationContract;
use Psr\Http\Message\ResponseInterface;

abstract class AbstractHttpRepository implements RepositoryContract
{
    protected ClientInterface $client;

    public function __construct(ClientInterface $client) {
        $this->client = $client;
    }

    /**
     * @param SpecificationContract $specification
     * @return Iterator
     * @throws ConnectException|InvalidDataException
     */
    final public function query(SpecificationContract $specification): Iterator
    {
        $queryOptions = $specification->specify($this->client->getConfig());

        try {
            $this->validate($queryOptions);
            $response = $this->request($queryOptions);

            return $this->convertToIterator($response, $queryOptions);
        } catch (GuzzleException $e) {
            throw new ConnectException($e->getMessage(), $e->getCode(), $e->getPrevious());
        } catch (JsonException | InvalidArgumentException $e) {
            throw new InvalidDataException($e->getMessage(), $e->getCode(), $e->getPrevious());
        }
    }

    /**
     * @param array $queryOptions
     * @return ResponseInterface
     * @throws GuzzleException
     */
    abstract protected function request(array $queryOptions): ResponseInterface;

    /**
     * @param array $queryOptions
     * @throws InvalidArgumentException
     */
    abstract protected function validate(array $queryOptions): void;

    /**
     * @param ResponseInterface $response
     * @param array $queryOptions
     * @return Iterator
     * @throws JsonException|InvalidArgumentException
     */
    abstract protected function convertToIterator(ResponseInterface $response, array $queryOptions): Iterator;
}