<?php

declare(strict_types=1);

namespace Justenj\Commissions\Infrastructure\Repositories;

use Iterator;
use Justenj\Commissions\Application\Repositories\RepositoryContract;
use Justenj\Commissions\Application\Repositories\Country\RepositoryContract as CountryRepository;
use Justenj\Commissions\Application\Repositories\Rate\RepositoryContract as RateRepositoryContract;
use Justenj\Commissions\Application\Repositories\SpecificationContract;
use Psr\SimpleCache\CacheInterface;

/**
 * Decorator for a repository
 */
class ResponseCache implements RepositoryContract, CountryRepository, RateRepositoryContract
{
    private RepositoryContract $repository;
    private CacheInterface $cache;

    public function __construct(RepositoryContract $repository, CacheInterface $cache)
    {
        $this->repository = $repository;
        $this->cache = $cache;
    }

    public function query(SpecificationContract $specification): Iterator
    {
        $key = sha1(serialize($specification));

        try {
            if ($this->cache->has($key)) {
                return unserialize($this->cache->get($key));
            }
        } catch (\Psr\SimpleCache\InvalidArgumentException $e) { // Catch exception here because exceptions should not propagate on above levels
            // here should be logger
        }

        $iterator = $this->repository->query($specification);
        try {
            $this->cache->set($key, serialize($iterator));
        } catch (\Psr\SimpleCache\InvalidArgumentException $e) { // Catch exception here because exceptions should not propagate on above levels
            // here should be logger
        }

        return $iterator;
    }
}