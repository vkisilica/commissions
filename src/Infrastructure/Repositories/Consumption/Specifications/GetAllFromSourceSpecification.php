<?php

declare(strict_types=1);

namespace Justenj\Commissions\Infrastructure\Repositories\Consumption\Specifications;

use Justenj\Commissions\Application\Repositories\SpecificationContract;

class GetAllFromSourceSpecification implements SpecificationContract
{
    /** @var mixed */
    private $source;

    /**
     * @param mixed $source
     */
    public function __construct($source)
    {
        $this->source = $source;
    }

    /**
     * @param array $payload
     * @return array
     */
    public function specify($payload)
    {
        $payload['filePath'] = $this->source;
        return $payload;
    }
}