<?php

declare(strict_types=1);

namespace Justenj\Commissions\Infrastructure\Repositories\Consumption;

use Justenj\Commissions\Application\Repositories\Consumption\SpecificationFactoryContract;
use Justenj\Commissions\Application\Repositories\SpecificationContract;
use Justenj\Commissions\Infrastructure\Repositories\Consumption\Specifications\GetAllFromSourceSpecification;

class SpecificationFactory implements SpecificationFactoryContract
{
    /**
     * @param mixed $source
     * @return SpecificationContract
     */
    public function createGetAllFromSourceSpecification($source): SpecificationContract
    {
        return new GetAllFromSourceSpecification($source);
    }
}