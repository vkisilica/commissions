<?php

declare(strict_types=1);

namespace Justenj\Commissions\Infrastructure\Repositories\Consumption;

use Iterator;
use InvalidArgumentException;
use Justenj\Commissions\Application\Repositories\ConnectException;
use Justenj\Commissions\Application\Repositories\InvalidDataException;
use Justenj\Commissions\Application\Repositories\Consumption\RepositoryContract;
use Justenj\Commissions\Application\Repositories\SpecificationContract;
use Justenj\Commissions\Domain\Consumption;
use Justenj\Commissions\Domain\Country;
use Justenj\Commissions\Application\Repositories\Country\RepositoryContract as CountryRepositoryContract;
use Justenj\Commissions\Application\Repositories\Country\SpecificationFactoryContract as CountrySpecificationFactoryContract;
use JsonException;

class TxtFileRepository implements RepositoryContract
{
    private CountryRepositoryContract $countryRepository;
    private CountrySpecificationFactoryContract $countrySpecificationFactory;

    public function __construct(CountryRepositoryContract $countryRepository, CountrySpecificationFactoryContract $countrySpecificationFactory)
    {
        $this->countryRepository = $countryRepository;
        $this->countrySpecificationFactory = $countrySpecificationFactory;
    }

    /**
     * @param SpecificationContract $specification
     * @return Iterator
     * @throws \Justenj\Commissions\Application\Repositories\InvalidDataException
     * @throws \Justenj\Commissions\Application\Repositories\ConnectException
     */
    public function query(SpecificationContract $specification): Iterator
    {
        $specifications = $specification->specify([]);

        if (!is_array($specifications)) {
            throw new ConnectException('Specification should return array');
        }

        if (!array_key_exists('filePath', $specifications)) {
            throw new ConnectException('File path is not defined');
        }
        $filePath = $specifications['filePath'];

        if (!file_exists($filePath)) {
            throw new ConnectException(sprintf('File "%s" is not found', $filePath));
        }

        $handle = fopen($filePath, "r");
        if (!$handle) {
            throw new ConnectException(sprintf('Error while reading "%s"', $filePath));
        }

        while (($buffer = fgets($handle)) !== false) {
            try {
                $data = json_decode($buffer, true, 512, JSON_THROW_ON_ERROR);

                if (!array_key_exists('bin', $data)) {
                    throw new InvalidDataException('BIN key is required');
                }
                $bin = $data['bin'];

                $specification = $this->countrySpecificationFactory->createGetByBinSpecification($bin);
                /** @var Country $issueCountry */
                $issueCountry = $this->countryRepository->query($specification)->current();
                $data['country'] = $issueCountry->getAlpha2();
                yield Consumption::fromArray($data);
            } catch (JsonException | InvalidArgumentException $e) {
                throw new InvalidDataException($e->getMessage(), $e->getCode(), $e->getPrevious());
            }
        }
        if (!feof($handle)) {
            throw new ConnectException(sprintf('File pointer is not in the end of the "%s" file', $filePath));
        }
        if (!fclose($handle)) {
            throw new ConnectException(sprintf('Error while closing "%s"', $filePath));
        }
    }
}