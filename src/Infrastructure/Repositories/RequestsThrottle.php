<?php

declare(strict_types=1);

namespace Justenj\Commissions\Infrastructure\Repositories;

use Carbon\Carbon;
use Iterator;
use Justenj\Commissions\Application\Repositories\RepositoryContract;
use Justenj\Commissions\Application\Repositories\Country\RepositoryContract as CountryRepository;
use Justenj\Commissions\Application\Repositories\SpecificationContract;

/**
 * Decorator for a repository
 */
class RequestsThrottle implements RepositoryContract, CountryRepository
{
    private RepositoryContract $repository;
    private Carbon $lastRequestAt;
    private int $microsecondsBetweenRequests;

    public function __construct(RepositoryContract $repository, int $requests, int $microseconds)
    {
        $this->repository = $repository;

        $this->microsecondsBetweenRequests = (int)ceil($microseconds / $requests);
        $this->lastRequestAt = Carbon::now()->subMicroseconds($this->microsecondsBetweenRequests);
    }

    public function query(SpecificationContract $specification): Iterator
    {
        $diff = Carbon::now()->diffInMicroseconds($this->lastRequestAt);
        if ($diff < $this->microsecondsBetweenRequests) {
            usleep($this->microsecondsBetweenRequests - $diff);
        }
        $response = $this->repository->query($specification);
        $this->lastRequestAt = Carbon::now();

        return $response;
    }
}