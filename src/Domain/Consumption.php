<?php

declare(strict_types=1);

namespace Justenj\Commissions\Domain;

use Evp\Component\Money\Money;
use Webmozart\Assert\Assert;

final class Consumption
{
    private Bin $bin;
    private Country $issuingCountry;
    private Money $money;

    public function __construct(Bin $bin, Country $country, Money $money)
    {
        $this->bin = $bin;
        $this->issuingCountry = $country;
        $this->money = $money;
    }

    public static function fromArray(array $data): self
    {
        Assert::keyExists($data, 'bin');
        Assert::keyExists($data, 'country');
        Assert::keyExists($data, 'amount');
        Assert::keyExists($data, 'currency');

        $bin = new Bin($data['bin']);
        $country = Country::fromAlpha2($data['country']);
        $money = new Money($data['amount'], $data['currency']);

        return new self($bin, $country, $money);
    }

    public function getBin(): Bin
    {
        return $this->bin;
    }

    public function getCountry(): Country
    {
        return $this->issuingCountry;
    }

    public function getMoney(): Money
    {
        return $this->money;
    }

    public function getArrayRepresentation(): array
    {
        return [
            'bin' => $this->bin->getBin(),
            'issuingCountry' => [
                'alpha2' => $this->issuingCountry->getAlpha2()
            ],
            'money' => $this->getMoney()->getArrayRepresentation()
        ];
    }
}