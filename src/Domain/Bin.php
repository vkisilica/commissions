<?php

declare(strict_types=1);

namespace Justenj\Commissions\Domain;

use Webmozart\Assert\Assert;

final class Bin
{
    private string $bin;

    public function __construct(string $bin)
    {
        Assert::minLength($bin, 6);
        Assert::maxLength($bin, 8);
        Assert::digits($bin);

        $this->bin = $bin;
    }

    public function getBin(): string
    {
        return $this->bin;
    }
}