<?php

declare(strict_types=1);

namespace Justenj\Commissions\Domain\Commission;

use Evp\Component\Money\Money;
use Justenj\Commissions\Domain\Consumption;
use Justenj\Commissions\Domain\Exchange\ExchangeService;

final class CommissionService
{
    private ExchangeService $exchangeService;

    public function __construct(ExchangeService $exchangeService)
    {
        $this->exchangeService = $exchangeService;
    }

    public function calculate(Consumption $consumption): Money
    {
        $money = $consumption->getMoney();
        $consumptionCurrency = $money->getCurrency();

        if ($consumptionCurrency !== 'EUR') {
            $money = $this->exchangeService->exchange($money, 'EUR');
        }

        if ($consumption->getCountry()->isEU()) {
            $commission = $money->mul(0.01)->ceil();
        } else {
            $commission = $money->mul(0.02)->ceil();
        }

        return $commission;
    }
}