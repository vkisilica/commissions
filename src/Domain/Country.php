<?php

declare(strict_types=1);

namespace Justenj\Commissions\Domain;

use Webmozart\Assert\Assert;

final class Country
{
    private string $alpha2;

    private function __construct(string $alpha2)
    {
        $this->alpha2 = $alpha2;
    }

    public static function fromAlpha2(string $alpha2): self
    {
        Assert::length($alpha2, 2);

        return new self(mb_strtoupper($alpha2));
    }

    public function isEU(): bool
    {
        switch ($this->alpha2) {
            case 'AT':
            case 'BE':
            case 'BG':
            case 'CY':
            case 'CZ':
            case 'DE':
            case 'DK':
            case 'EE':
            case 'ES':
            case 'FI':
            case 'FR':
            case 'GR':
            case 'HR':
            case 'HU':
            case 'IE':
            case 'IT':
            case 'LT':
            case 'LU':
            case 'LV':
            case 'MT':
            case 'NL':
            case 'PO':
            case 'PT':
            case 'RO':
            case 'SE':
            case 'SI':
            case 'SK':
                $result = true;
                break;
            default:
                $result = false;
                break;
        }

        return $result;
    }

    public function getAlpha2(): string
    {
        return $this->alpha2;
    }
}