<?php

declare(strict_types=1);

namespace Justenj\Commissions\Domain\Exchange;

use DomainException;

class ExchangeRateNotDefinedException extends DomainException
{

}