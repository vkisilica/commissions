<?php

declare(strict_types=1);

namespace Justenj\Commissions\Domain\Exchange;

use Webmozart\Assert\Assert;

final class Rate
{
    private string $fromCurrency;
    private string $toCurrency;
    private float $rate;

    public function __construct(string $fromCurrency, string $toCurrency, float $rate)
    {
        Assert::length($fromCurrency, 3);
        Assert::length($toCurrency, 3);

        Assert::greaterThan($rate, 0);

        $this->fromCurrency = mb_strtoupper($fromCurrency);
        $this->toCurrency = mb_strtoupper($toCurrency);
        $this->rate = $rate;
    }

    public function getPair(): string
    {
        return sprintf("%s/%s", $this->toCurrency, $this->fromCurrency);
    }

    public function getRate(): float
    {
        return $this->rate;
    }
}