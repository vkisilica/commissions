<?php

declare(strict_types=1);

namespace Justenj\Commissions\Domain\Exchange;

use Evp\Component\Money\Money;
use Iterator;

final class ExchangeService
{
    private array $rates;

    public function __construct(Iterator $rates)
    {
        $this->rates = [];
        /** @var Rate $rate */
        foreach ($rates as $rate) {
            $this->rates[$rate->getPair()] = $rate->getRate();
        }
    }

    public function exchange(Money $money, string $toCurrency): Money
    {
        $currentCurrency = $money->getCurrency();
        $isSameCurrency = $currentCurrency === $toCurrency;
        if ($isSameCurrency) {
            return $money;
        }

        $pairKey = $toCurrency . '/' . $currentCurrency;
        if (!array_key_exists($pairKey, $this->rates)) {
            throw new ExchangeRateNotDefinedException(sprintf('"%s" pair is not defined', $pairKey));
        }

        return new Money($money->div($this->rates[$pairKey])->getAmount(), $toCurrency);
    }
}