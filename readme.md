Guys, I am really sorry for not doing the test in the way you asked, but this test was so good for DDD implementation.

The main requirements are completed as you asked.

Hope you still want to conduct an interview with me.
  
## Task description
[task.md](task.md)

## Some explains about solution

### Use case classes
Usually, I prefer to develop within business processes. Use cases are thins that help me in that. One use case is 
one business operation. Also, I prefer to cover each use case by functional tests.

See `CalculateCommissionsInEurosForAllConsumptions`

### Generators
Why did I use generators into the consumption repository and into the CalculateCommissionsInEurosForAllConsumptions use 
case? 

Because of memory limit. We don't know how huge input.txt can be.

Run the script for input.txt. The file contains 6 entries.
```
php examples/nice.php examples/assets/input.txt
```
Script uses 6.291.456 bytes of memory

Then, run the script for input_100.txt. The file contains 100 entries.
```
php examples/nice.php examples/assets/input_100.txt
```
Script uses 6.291.456 bytes. The same amount.

Now, run the script with the big.txt file. The file contains more than 100.000 entries
```
php examples/nice.php examples/assets/big.txt
```
Still, 6.291.456 bytes.

### Specification classes
The interface doesn't contain the "specify" method because we usually change implementation of specifications together
with repositories.

### The RequestsThrottle class
I implement this decorator for bin repository because the binlist service has limits. 10 requests per minute.
The RequestsThrottle class is used together with the ResponseCache decorator and they both cover the country repository.
It looks like:
```
$countryRpository = 
new ResponseCache(
    new RequestsThrottle(
        new CountryRepository(),
        10,
        1000000
    ), 
    $someCacheImplementation
)
$countries = $countryRpository->query($specification);
```

### evp/money library
I like this library because there are used fractions and bcmath, but there also is one disadvantage. 
It is the "setAmount" method, and that means an instance of the Money class is mutable. It should be immutable.