<?php

declare(strict_types=1);

require_once (__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php');

$container = new \League\Container\Container();

$container->add(\Justenj\Commissions\Application\Repositories\Country\RepositoryContract::class, function () use ($container) {
    $client = new \GuzzleHttp\Client([
        'base_uri' => 'https://lookup.binlist.net'
    ]);

    return new \Justenj\Commissions\Infrastructure\Repositories\ResponseCache(
        new \Justenj\Commissions\Infrastructure\Repositories\RequestsThrottle(
            new \Justenj\Commissions\Infrastructure\Repositories\Country\HttpRepository(
                $client
            ),
            10,
            60000000
        ),
        $container->get(\Psr\SimpleCache\CacheInterface::class)
    );
});

$container->add(\Justenj\Commissions\Application\Repositories\Rate\RepositoryContract::class, function () use ($container) {
    $client = new \GuzzleHttp\Client([
        'base_uri' => 'https://api.exchangeratesapi.io/latest'
    ]);
    return new \Justenj\Commissions\Infrastructure\Repositories\ResponseCache(
        new \Justenj\Commissions\Infrastructure\Repositories\Rate\HttpRepository($client),
        $container->get(\Psr\SimpleCache\CacheInterface::class)
    );
});

$container->add(\Psr\SimpleCache\CacheInterface::class, \Cache\Adapter\PHPArray\ArrayCachePool::class);
$container->add(\Justenj\Commissions\Application\Repositories\Country\SpecificationFactoryContract::class, \Justenj\Commissions\Infrastructure\Repositories\Country\SpecificationFactory::class);
$container->add(\Justenj\Commissions\Application\Repositories\Rate\SpecificationFactoryContract::class, \Justenj\Commissions\Infrastructure\Repositories\Rate\SpecificationFactory::class);
$container->add(\Justenj\Commissions\Application\Repositories\Consumption\RepositoryContract::class, \Justenj\Commissions\Infrastructure\Repositories\Consumption\TxtFileRepository::class)
    ->addArgument(\Justenj\Commissions\Application\Repositories\Country\RepositoryContract::class)
    ->addArgument(\Justenj\Commissions\Application\Repositories\Country\SpecificationFactoryContract::class);
$container->add(\Justenj\Commissions\Application\Repositories\Consumption\SpecificationFactoryContract::class, \Justenj\Commissions\Infrastructure\Repositories\Consumption\SpecificationFactory::class);

$container->add(\Justenj\Commissions\Application\Repositories\Rate\SpecificationFactoryContract::class, \Justenj\Commissions\Infrastructure\Repositories\Rate\SpecificationFactory::class);

$container->add(\Justenj\Commissions\Application\UseCases\CalculateCommissionsInEurosForAllConsumptions::class)
    ->addArgument(\Justenj\Commissions\Application\Repositories\Consumption\RepositoryContract::class)
    ->addArgument(\Justenj\Commissions\Application\Repositories\Consumption\SpecificationFactoryContract::class)
    ->addArgument(\Justenj\Commissions\Application\Repositories\Rate\RepositoryContract::class)
    ->addArgument(\Justenj\Commissions\Application\Repositories\Rate\SpecificationFactoryContract::class);