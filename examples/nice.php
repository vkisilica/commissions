<?php

declare(strict_types=1);

require_once ('bootstrap.php');

$useCase = $container->get(\Justenj\Commissions\Application\UseCases\CalculateCommissionsInEurosForAllConsumptions::class);

if (!array_key_exists(1, $argv)) {
    throw new \Exception('Filepath is required');
}

$commissions = $useCase->handle($argv[1]);

foreach ($commissions as $commission) {
    echo $commission['commission']['amount'] . PHP_EOL;
}

//echo memory_get_peak_usage(true) . ' bytes' . PHP_EOL;